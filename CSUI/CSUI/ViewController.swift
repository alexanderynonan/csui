//
//  ViewController.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit
import CSUtilities

class ViewController: UIViewController {

    @IBOutlet weak var txt: CSUITextField!
    @IBOutlet weak var lbl: CSUILabel!
    
    @IBAction func clickBtnCloseKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl.text = ""//Esto es un mensaje de prueba Esto es un mensaje de prueba Esto es un mensaje de prueba"
    }
    
    deinit {
        print("SE VA")
    }
    
}

extension ViewController: CSUITextFieldDelegate{
    
    func textFieldUserDidEndEditing(_ textField: CSUITextField) {
        
        if (textField.text?.count ?? 0) == 0 {
            textField.hideErrorMessage()
        }else if !(textField.text?.mailIsValid() ?? false) {
            textField.showErrorMessageWithText("Ingresa un mail válido y no seas chivito como Walter Ingresa un mail válido y no seas chivito como Walter")
        }
    }
    
    func textField(_ textField: CSUITextField, tapInLeftIconButton button: UIButton) {
        
        let image = button.tag == 0 ? UIImage(named: "ic_showPassword_1") : UIImage(named: "ic_back_key")
        button.setImage(image, for: .normal)
        button.tag = button.tag == 0 ? 1 : 0
    }
    
    func textField(_ textField: CSUITextField, tapInRightIconButton button: UIButton) {
        

    }
}

