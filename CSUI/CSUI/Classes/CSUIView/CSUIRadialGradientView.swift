//
//  CSUIRadialGradientView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

fileprivate class CSUIRadialGradientLayer: CALayer {

    public var insideColor      : UIColor = UIColor.clear
    public var outSideColor     : UIColor = UIColor.clear
    public var gradiantCenter   : CGPoint = .zero
    public var startRadius      : CGFloat = 0.0
    public var endRadius        : CGFloat = 0.0
    
    private func createGradient(in ctx: CGContext){

        let colors = [self.insideColor.cgColor, self.outSideColor.cgColor] as CFArray

        let endGradientRadius = min(frame.width, frame.height) * self.endRadius
        let startGradientRadius = min(frame.width, frame.height) * self.startRadius

        let width = bounds.size.width
        let height = bounds.size.height

        let center = CGPoint(x: width / 2 + (width / 2 * self.gradiantCenter.x), y: height / 2 + (height / 2 * self.gradiantCenter.y))
        guard let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil) else { return }

        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: startGradientRadius, endCenter: center, endRadius: endGradientRadius, options: [.drawsAfterEndLocation , .drawsBeforeStartLocation])
    }

    override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }

    required init(coder aDecoder: NSCoder) {
        super.init()
    }

    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        self.createGradient(in: ctx)
    }

}

@IBDesignable open class CSUIRadialGradientView: CSUIShadowView {
    
    @IBInspectable public var insideColor   : UIColor = UIColor.clear
    @IBInspectable public var outSideColor  : UIColor = UIColor.clear
    
    @IBInspectable public var gradiantCenter : CGPoint{
        set(newValue){
            self.internalGradiantCenter.x = newValue.x > 1 ? 1 : newValue.x < -1 ? -1 : newValue.x
            self.internalGradiantCenter.y = newValue.y > 1 ? 1 : newValue.y < -1 ? -1 : newValue.y
        }
        get{
            return self.internalGradiantCenter
        }
    }
    
    @IBInspectable public var startRadius : CGFloat{
        set(newValue){
            if newValue > 1 {
                self.internalStartRadius = 1
            }else if newValue < 0 {
                self.internalStartRadius = 0
            }else{
                self.internalStartRadius = newValue
            }
        }
        get{
            return self.internalStartRadius
        }
    }
    
    @IBInspectable public var endRadius : CGFloat{
        set(newValue){
            if newValue > 1 {
                self.internalEndRadius = 1
            }else if newValue < 0 {
                self.internalEndRadius = 0
            }else{
                self.internalEndRadius = newValue
            }
        }
        get{
            return self.internalEndRadius
        }
    }
    
    fileprivate var internalGradiantCenter  = CGPoint.zero
    fileprivate var internalStartRadius     : CGFloat = 0
    fileprivate var internalEndRadius       : CGFloat = 0.5
    
    private lazy var gradientLayer : CSUIRadialGradientLayer = {
        
        let gradient = CSUIRadialGradientLayer()
        gradient.frame = bounds
        gradient.insideColor = self.insideColor
        gradient.outSideColor = self.outSideColor
        gradient.gradiantCenter = self.gradiantCenter
        gradient.startRadius = self.startRadius
        gradient.endRadius = self.endRadius
        return gradient
    }()
    
    open override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        self.layer.insertSublayer(self.gradientLayer, at: 0)
        self.arrayAditionalLayers.insert(self.gradientLayer)
        self.setupCorners()
    }
    
    open override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        self.layer.insertSublayer(self.gradientLayer, at: 0)
        self.arrayAditionalLayers.insert(self.gradientLayer)
        self.setupCorners()
    }
}
