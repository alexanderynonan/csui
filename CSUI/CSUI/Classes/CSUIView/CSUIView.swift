//
//  CSUIView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

extension UIView {

    @IBInspectable public var parallaxEffect : Bool{
        get{
            return Parallax.hasEffect
        }
        set(newValue){
            Parallax.hasEffect = newValue

            if Parallax.hasEffect == true {
                self.addMotionEffect(Parallax.effectGroup)
            }else{
                self.removeMotionEffect(Parallax.effectGroup)
            }
        }
    }

    @IBInspectable public var minHorizontal : CGFloat{
        get{
            return Parallax.xMotion.minimumRelativeValue as? CGFloat ?? 0.0
        }
        set(newValue){
            Parallax.xMotion.minimumRelativeValue = newValue
            Parallax.effectGroup.motionEffects = [Parallax.xMotion, Parallax.yMotion]
        }
    }

    @IBInspectable public var maxHorizontal : CGFloat{
        get{
            return Parallax.xMotion.maximumRelativeValue as? CGFloat ?? 0.0
        }
        set(newValue){
            Parallax.xMotion.maximumRelativeValue = newValue
            Parallax.effectGroup.motionEffects = [Parallax.xMotion, Parallax.yMotion]
        }
    }

    @IBInspectable public var minVertical : CGFloat{
        get{
            return Parallax.yMotion.minimumRelativeValue as? CGFloat ?? 0.0
        }
        set(newValue){
            Parallax.yMotion.minimumRelativeValue = newValue
            Parallax.effectGroup.motionEffects = [Parallax.xMotion, Parallax.yMotion]
        }
    }

    @IBInspectable public var maxVertical : CGFloat{
        get{
            return Parallax.yMotion.maximumRelativeValue as? CGFloat ?? 0.0
        }
        set(newValue){
            Parallax.yMotion.maximumRelativeValue = newValue
            Parallax.effectGroup.motionEffects = [Parallax.xMotion, Parallax.yMotion]
        }
    }

    private struct Parallax {

        static var hasEffect    = false
        static let xMotion      = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        static let yMotion      = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        static let effectGroup  = UIMotionEffectGroup()
    }

    public func addMotionEffectWithPath(_ path: String, minValue: Any, maxValue: Any, axis: UIInterpolatingMotionEffect.EffectType) {

        let motionEffect = UIInterpolatingMotionEffect(keyPath: path, type: .tiltAlongHorizontalAxis)
        motionEffect.minimumRelativeValue = minValue
        motionEffect.maximumRelativeValue = maxValue
        self.addMotionEffect(motionEffect)
    }

    public func removeMotionEffectWithPath(_ keyPath : String) {
        self.motionEffects.removeAll(where: {($0 as? UIInterpolatingMotionEffect)?.keyPath == keyPath})
    }

    public var parallaxKeys : [String]{
        get{
            return self.motionEffects.map({ "\(($0 as? UIInterpolatingMotionEffect)?.keyPath ?? "")" })
        }
    }
}
