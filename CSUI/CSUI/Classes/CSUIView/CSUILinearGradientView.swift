//
//  CSUILinearGradientView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUILinearGradientView: CSUIShadowView {
    
    @IBInspectable public var startColor    : UIColor{
        get{
            return self.internalStartColor
        }set{
            self.internalStartColor = newValue
            self.updateGradientColor()
        }
    }
    @IBInspectable public var endColor      : UIColor{
        get{
            return self.internalEndColor
        }set{
            self.internalEndColor = newValue
            self.updateGradientColor()
        }
    }
    
    @IBInspectable public var startGradiant : CGPoint{
        set(newValue){
            self.internalStartGradiant.x = newValue.x > 1 ? 1 : newValue.x < -1 ? -1 : newValue.x
            self.internalStartGradiant.y = newValue.y > 1 ? 1 : newValue.y < -1 ? -1 : newValue.y
            self.updateGradientPosition()
        }
        get{
            return self.internalStartGradiant
        }
    }
    
    @IBInspectable public var endGradiant : CGPoint{
        set(newValue){
            self.internalEndGradiant.x = newValue.x > 1 ? 1 : newValue.x < -1 ? -1 : newValue.x
            self.internalEndGradiant.y = newValue.y > 1 ? 1 : newValue.y < -1 ? -1 : newValue.y
            self.updateGradientPosition()
        }
        get{
            return self.internalEndGradiant
        }
    }
    
    private var internalStartGradiant   = CGPoint.zero
    private var internalEndGradiant     = CGPoint.zero
    private var internalStartColor      = UIColor.clear
    private var internalEndColor        = UIColor.clear
    
    lazy var gradientLayer: CAGradientLayer = {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        return gradientLayer
    }()
    
    private func updateGradientColor(){
        self.gradientLayer.colors = [self.internalStartColor.cgColor, self.internalEndColor.cgColor]
    }
    
    private func updateGradientPosition(){
        
        let startCenter = CGPoint(x: (self.internalStartGradiant.x + 1)/2, y: (self.internalStartGradiant.y + 1)/2)
        let endCenter = CGPoint(x: (self.internalEndGradiant.x + 1)/2, y: (self.internalEndGradiant.y + 1)/2)
        self.gradientLayer.startPoint = startCenter
        self.gradientLayer.endPoint = endCenter
    }
    
    override public func prepareForInterfaceBuilder() {

        super.prepareForInterfaceBuilder()
        self.layer.insertSublayer(self.gradientLayer, at: 0)
        self.arrayAditionalLayers.insert(self.gradientLayer)
        self.setupCorners()
    }
}
