//
//  CSUITabBar.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable class CSUITabBar: UITabBar {

    @IBInspectable public var cornerRadius : CGFloat = 0.0
    
    @IBInspectable var color: UIColor?{
        get{
            return UIColor.white
        }
        set(newValue){
            self.backgroundColor = newValue
        }
    }
    
    @IBInspectable public var shadowColor : UIColor = UIColor.black{
        didSet{
            self.addShape()
        }
    }


    @IBInspectable public var shadowOffset : CGSize = CGSize(width: 0.9, height: 0.9){
        didSet{
            self.addShape()
        }
    }

    @IBInspectable public var shadowOpacity : Float = 1{
        didSet{
            
            self.addShape()
        }
    }

    
    public func setShadowStyle() {
        self.shapeLayer?.shadowColor      = self.shadowColor.cgColor
        self.shapeLayer?.shadowOffset     = self.shadowOffset
        self.shapeLayer?.shadowOpacity    = self.shadowOpacity
        self.layer.masksToBounds          = false
    }
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setShadowStyle()
    }

    private var shapeLayer: CALayer?
    
    override func draw(_ rect: CGRect) {
        self.addShape()
    }
    
    private func addShape() {
        
        let shapeLayer = CAShapeLayer()

        shapeLayer.path = createPath()
        shapeLayer.shadowColor = self.shadowColor.cgColor
        shapeLayer.shadowOffset = self.shadowOffset
        shapeLayer.shadowOpacity = self.shadowOpacity
        shapeLayer.strokeColor = UIColor.gray.withAlphaComponent(0.1).cgColor
        shapeLayer.fillColor = self.color?.cgColor ?? UIColor.white.cgColor
        shapeLayer.lineWidth = 1

        if let oldShapeLayer = self.shapeLayer {
            layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        } else {
            layer.insertSublayer(shapeLayer, at: 0)
        }

        self.shapeLayer = shapeLayer
    }
    
    
    private func createPath() -> CGPath {
        let path = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: [.topLeft, .topRight],
            cornerRadii: CGSize(width: self.cornerRadius, height: 0.0))
        return path.cgPath
    }


}
