//
//  CSUITextButton.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit


@IBDesignable open class CSUITextButton: UIButton {
    
    @IBInspectable public var underLineText : Bool{
        get{
            return self.internalUnderLineText
        }
        set(newValue){
            self.internalUnderLineText = newValue
            self.createUnderlineText()
        }
    }
    
    @IBInspectable public var spaceLine : CGFloat{
        get{
            return self.internalSpaceLine
        }
        set(newValue){
            self.internalSpaceLine = newValue
            self.setSpaceLineInText()
        }
    }

    private var internalUnderLineText   = false
    private var internalSpaceLine       : CGFloat = 0.0
    
    fileprivate func createUnderlineText(){
    
        guard let oldAttributes = self.titleLabel?.attributedText else { return }
        let textContent = self.titleLabel?.text ?? ""
        let newAttributes = NSMutableAttributedString(attributedString: oldAttributes)
        let range = NSRange(location: 0, length: self.internalUnderLineText ? textContent.count : 0)
        newAttributes.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        self.titleLabel?.attributedText = newAttributes
    }
    
    fileprivate func setSpaceLineInText(){

        guard let oldAttributes = self.titleLabel?.attributedText else { return }
        var range = NSRange(location: 0, length: oldAttributes.length)
        
        guard let paragraphStyle = oldAttributes.attribute(NSAttributedString.Key.paragraphStyle, at: 0, effectiveRange: &range) as? NSMutableParagraphStyle else { return }
        paragraphStyle.lineSpacing = self.internalSpaceLine

        let attributedString = NSMutableAttributedString(attributedString: oldAttributes)
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: range)

        self.titleLabel?.attributedText = attributedString
    }
}
