//
//  CSUIButton.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIButton: CSUIShadowButton {

    @IBOutlet public var autoGoneConstraints: [NSLayoutConstraint]?
    
    open override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        self.setAutoGone()
    }
    
    private func setAutoGone(){
        if self.titleLabel?.text?.count == 0 {
            for constraint in self.autoGoneConstraints ?? []{
                constraint.constant = 0
            }
        }
    }
    
    public func setGone(){
        for constraint in self.autoGoneConstraints ?? []{
            constraint.constant = 0
        }
    }
}
