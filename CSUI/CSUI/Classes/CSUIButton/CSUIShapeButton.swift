//
//  CSUIShapeButton.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIShapeButton: CSUITextButton {
    
    @IBInspectable public var borderColor : UIColor{
        
        get{
            if let color = self.layer.borderColor{
                return UIColor(cgColor: color)
            }else{
                return UIColor.black
            }
        }
        set(newValue){
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable public var borderWidth : CGFloat{
        get{
            return self.layer.borderWidth
        }
        set(newValue){
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable public var cornerRadius : CGFloat{
        get{
            return self.layer.cornerRadius
        }
        set(newValue){
            self.layer.cornerRadius = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var rightTop : Bool{
        get{
            return self.internalRightTop
        }
        set(newValue){
            self.internalRightTop = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var leftTop : Bool{
        get{
            return self.internalLeftTop
        }
        set(newValue){
            self.internalLeftTop = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var rightDown : Bool{
        get{
            return self.internalRightDown
        }
        set(newValue){
            self.internalRightDown = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var leftDown : Bool{
        get{
            return self.internalLeftDown
        }
        set(newValue){
            self.internalLeftDown = newValue
            self.setupCorners()
        }
    }
    
    private var internalRightTop        = false
    private var internalRightDown       = false
    private var internalLeftTop         = false
    private var internalLeftDown        = false
    public  var arrayAditionalLayers    = Set<CALayer>()
    
    private func setupCorners() {
        
        let maskedCorners           = self.getMaskedCorners()
        self.layer.maskedCorners    = maskedCorners
        
        for subLayer in self.arrayAditionalLayers {
            
            subLayer.cornerRadius  = self.cornerRadius
            subLayer.maskedCorners = maskedCorners
        }
    }
    
    public func getMaskedCorners() -> CACornerMask {
        
        var arrayCornerMask = [CACornerMask]()
        
        if self.leftTop     {arrayCornerMask.append(.layerMinXMinYCorner)}
        if self.rightTop    {arrayCornerMask.append(.layerMaxXMinYCorner)}
        if self.leftDown    {arrayCornerMask.append(.layerMinXMaxYCorner)}
        if self.rightDown   {arrayCornerMask.append(.layerMaxXMaxYCorner)}
        
        return arrayCornerMask.count != 0 ? CACornerMask(arrayCornerMask) : [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
}
