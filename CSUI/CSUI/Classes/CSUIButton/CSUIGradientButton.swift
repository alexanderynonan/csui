//
//  CSUIGradientButton.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIGradientButton: CSUIShapeButton {
    
    @IBInspectable public var gradientStartColor : UIColor{
        get{
            return self.internalGradientStartColor ?? .clear
        }set{
            self.internalGradientStartColor = newValue
            self.updateGradientColor()
        }
    }
    
    @IBInspectable public var gradientEndColor : UIColor{
        get{
            return self.internalGradientEndColor ?? .clear
        }set{
            self.internalGradientEndColor = newValue
            self.updateGradientColor()
        }
    }
    
    private var internalGradientStartColor  : UIColor?
    private var internalGradientEndColor    : UIColor?
    
    private lazy var gradiant: CAGradientLayer = {
       
        let gradiant = CAGradientLayer()
        gradiant.locations = [0.0, 1.0]
        gradiant.masksToBounds = true
        return gradiant
    }()
    
    public func updateGradientColor(){
        
        if let starColor = self.internalGradientStartColor, let endColor = self.internalGradientEndColor{
            
            self.layer.insertSublayer(self.gradiant, at: 0)
            self.arrayAditionalLayers.insert(self.gradiant)
            self.gradiant.frame = self.bounds
            self.gradiant.colors = [starColor.cgColor, endColor.cgColor]
            self.layoutIfNeeded()
        }else {
            self.gradiant.removeFromSuperlayer()
        }
    }
}
