//
//  CSUIImageView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIImageView: CSUIShadowImageView {
    
    @IBOutlet public var autoGoneConstraints: [NSLayoutConstraint]?

    public func setGone(){
        for constraint in self.autoGoneConstraints ?? []{
            constraint.constant = 0
        }
    }
    
    public func autoContentMode() {
        
        let widthImage = self.image?.size.width ?? 0
        let heightImage = self.image?.size.height ?? 0
        let widthContent = self.frame.width
        let heightContent = self.frame.height
        
        if widthImage > heightImage {
            let rateImage = widthImage / heightImage
            let rateContent = widthContent / heightContent
            
            if rateContent >= 1 && rateImage >= 1 {
                self.contentMode = .scaleAspectFill
            }else{
                self.contentMode = .scaleAspectFit
            }
        }else{
            let rateImage = heightImage / widthImage
            let rateContent = heightContent / widthContent
            
            if rateContent >= 1 && rateImage >= 1 {
                self.contentMode = .scaleAspectFill
            }else{
                self.contentMode = .scaleAspectFit
            }
        }
    }
}
