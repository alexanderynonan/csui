//
//  CSUIShapeImgeView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIShapeImgeView: UIImageView {
    
    @IBInspectable public var drawBorderLine: Bool{
        get{
            return self.internalDrawBorderLine
        }
        set(newValue){
            self.internalDrawBorderLine = newValue
            self.updateBorder()
            self.updateDashPatternLine()
        }
    }
    
    @IBInspectable public var lineWidth : Float{
        get{
            return self.internalLineWidth
        }
        set(newValue){
            self.internalLineWidth = newValue
            self.borderLayer.lineDashPattern = [NSNumber(value: self.internalLineWidth), NSNumber(value: self.internalLineSeparator)]
        }
    }
    
    @IBInspectable public var lineSeparator : Float{
        get{
            return self.internalLineSeparator
        }
        set(newValue){
            self.internalLineSeparator = newValue
            self.borderLayer.lineDashPattern = [NSNumber(value: self.internalLineWidth), NSNumber(value: self.internalLineSeparator)]
        }
    }
    
    @IBInspectable public var borderColor : UIColor{
        
        get{
            return self.internalBorderColor
        }
        set(newValue){
            self.internalBorderColor = newValue
            self.updateDashPatternLine()
        }
    }
    
    @IBInspectable public var borderWidth : CGFloat{
        get{
            return self.internalBorderWidth
        }
        set(newValue){
            self.internalBorderWidth = newValue
            self.updateDashPatternLine()
        }
    }
    
    @IBInspectable public var cornerRadius : CGFloat{
        get{
            return self.layer.cornerRadius
        }
        set(newValue){
            self.layer.cornerRadius = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var rightTop : Bool{
        get{
            return self.internalRightTop
        }
        set(newValue){
            self.internalRightTop = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var leftTop : Bool{
        get{
            return self.internalLeftTop
        }
        set(newValue){
            self.internalLeftTop = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var rightDown : Bool{
        get{
            return self.internalRightDown
        }
        set(newValue){
            self.internalRightDown = newValue
            self.setupCorners()
        }
    }
    
    @IBInspectable public var leftDown : Bool{
        get{
            return self.internalLeftDown
        }
        set(newValue){
            self.internalLeftDown = newValue
            self.setupCorners()
        }
    }
    
    private var internalDrawBorderLine  = false
    private var internalBorderColor     : UIColor = .clear
    private var internalBorderWidth     : CGFloat = 0
    private var internalRightTop        = false
    private var internalRightDown       = false
    private var internalLeftTop         = false
    private var internalLeftDown        = false
    private var internalLineWidth       : Float = 0.0
    private var internalLineSeparator   : Float = 0.0
    public  var borderLayer             = CAShapeLayer()
    public  var arrayAditionalLayers    = Set<CALayer>()
    
    public func setupCorners() {
        
        let maskedCorners           = self.getMaskedCorners()
        self.layer.maskedCorners    = maskedCorners
        
        for subLayer in self.arrayAditionalLayers {
            
            subLayer.cornerRadius  = self.cornerRadius
            subLayer.maskedCorners = maskedCorners
        }
    }
    
    private func updateDashPatternLine() {
        
        if self.drawBorderLine {
            self.borderLayer.strokeColor    = self.borderColor.cgColor
            self.borderLayer.lineWidth      = self.borderWidth
            self.layer.borderWidth          = 0
            self.layer.borderColor          = nil
        }else{
            self.layer.borderColor          = self.borderColor.cgColor
            self.layer.borderWidth          = self.borderWidth
        }
    }
    
    private func updateBorder(){
        
        if self.drawBorderLine {
            self.updateBorderDashLine()
        }else{
            self.borderLayer.removeFromSuperlayer()
        }
    }
    
    private func updateBorderDashLine() {
        
        let bounds      = CGRect(x: self.borderWidth/4, y: self.borderWidth/4, width: self.frame.width - self.borderWidth, height: self.frame.height - self.borderWidth)
        let cornerRadii = CGSize(width: self.cornerRadius, height: self.cornerRadius)
                
        self.borderLayer.frame              = bounds
        self.borderLayer.fillColor          = nil
        self.borderLayer.path               = UIBezierPath(roundedRect: bounds, byRoundingCorners: self.getRectCorners(), cornerRadii: cornerRadii).cgPath
        self.layer.insertSublayer(self.borderLayer, at: 0)
        self.arrayAditionalLayers.insert(self.borderLayer)
    }
    
    open override func prepareForInterfaceBuilder() {
    
        self.layer.masksToBounds = true
        super.prepareForInterfaceBuilder()
        self.updateBorder()
        self.setupCorners()
    }
}

extension CSUIShapeImgeView {
    
    public func getMaskedCorners() -> CACornerMask {
        
        var arrayCornerMask = [CACornerMask]()
        
        if self.leftTop     {arrayCornerMask.append(.layerMinXMinYCorner)}
        if self.rightTop    {arrayCornerMask.append(.layerMaxXMinYCorner)}
        if self.leftDown    {arrayCornerMask.append(.layerMinXMaxYCorner)}
        if self.rightDown   {arrayCornerMask.append(.layerMaxXMaxYCorner)}
        
        return arrayCornerMask.count != 0 ? CACornerMask(arrayCornerMask) : [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    public func getRectCorners() -> UIRectCorner {
        
        var arrayCornerMask = [UIRectCorner]()
        
        if self.leftTop     {arrayCornerMask.append(.topLeft)}
        if self.rightTop    {arrayCornerMask.append(.topRight)}
        if self.leftDown    {arrayCornerMask.append(.bottomLeft)}
        if self.rightDown   {arrayCornerMask.append(.bottomRight)}
        
        return arrayCornerMask.count != 0 ? UIRectCorner(arrayCornerMask) : .allCorners
    }
}

