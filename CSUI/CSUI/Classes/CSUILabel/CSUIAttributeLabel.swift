//
//  CSUIAttributeLabel.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUIAttributeLabel: UILabel {
 
    @IBInspectable public var spaceLine : CGFloat{
        get{
            return self.internalSpaceLine
        }
        set(newValue){
            self.internalSpaceLine = newValue
            self.setSpaceLineInText()
        }
    }
    
    @IBInspectable public var underLineText : Bool{
        get{
            return self.internalUnderLineText
        }
        set(newValue){
            self.internalUnderLineText = newValue
            self.createUnderlineText()
        }
    }

    @IBInspectable public var paddingTop : CGFloat{
        get{
            return self.paddingInsets.top
        }
        set(newValue){
            self.paddingInsets.top = newValue
        }
    }

    @IBInspectable public var paddingBottom : CGFloat{
        get{
            return self.paddingInsets.bottom
        }
        set(newValue){
            self.paddingInsets.bottom = newValue
        }
    }

    @IBInspectable public var paddingLeft : CGFloat{
        get{
            return self.paddingInsets.left
        }
        set(newValue){
            self.paddingInsets.left = newValue
        }
    }

    @IBInspectable public var paddingRight : CGFloat{
        get{
            return self.paddingInsets.right
        }
        set(newValue){
            self.paddingInsets.right = newValue
        }
    }

    private var internalUnderLineText   = false
    fileprivate var paddingInsets       = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    fileprivate var internalSpaceLine   : CGFloat = 0.0
    
    fileprivate func setSpaceLineInText(){
        
        guard let oldAttributes = self.attributedText, oldAttributes.length != 0 else { return }
        var range = NSRange(location: 0, length: oldAttributes.length)
        
        guard let paragraphStyle = oldAttributes.attribute(NSAttributedString.Key.paragraphStyle, at: 0, effectiveRange: &range) as? NSMutableParagraphStyle else { return }
        paragraphStyle.lineSpacing = self.internalSpaceLine

        let attributedString = NSMutableAttributedString(attributedString: oldAttributes)
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: range)

        self.attributedText = attributedString
    }

    public override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: self.paddingInsets))
    }

    public override var intrinsicContentSize: CGSize {
        get{
            var contentSize = super.intrinsicContentSize
            contentSize.height += self.paddingInsets.top + self.paddingInsets.bottom
            contentSize.width += self.paddingInsets.left + self.paddingInsets.right
            return contentSize
        }
    }
    
    fileprivate func createUnderlineText(){
    
        guard let oldAttributes = self.attributedText else { return }
        let textContent = self.text ?? ""
        let newAttributes = NSMutableAttributedString(attributedString: oldAttributes)
        let range = NSRange(location: 0, length: self.internalUnderLineText ? textContent.count : 0)
        newAttributes.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        self.attributedText = newAttributes
    }
}
