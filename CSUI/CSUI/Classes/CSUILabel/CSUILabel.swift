//
//  CSUILabel.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUILabel: CSUIShadowLabel {
    
    @IBOutlet public var autoGoneConstraints: [NSLayoutConstraint]?
    
    open override var text: String?{
        didSet{
            self.setAutoGone()
        }
    }
    
    private func setAutoGone(){
        if self.text?.count == 0 {
            for constraint in self.autoGoneConstraints ?? [] {
                constraint.constant = 0
            }
        }
    }
    
    public func setGone(){
        for constraint in self.autoGoneConstraints ?? []{
            constraint.constant = 0
        }
    }
}
