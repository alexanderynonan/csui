//
//  CSUIPageControl.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit


@IBDesignable open class CSUIPageControl: CSUIShadowPageControl {
    
    @IBInspectable public var iconActive: UIImage?{
        get{
            return self.internalIconActive
        }
        set{
            self.internalIconActive = newValue?.withRenderingMode(.alwaysTemplate)
        }
    }
    
    @IBInspectable public var iconInactive: UIImage?{
        get{
            return self.internalIconInactive
        }
        set{
            self.internalIconInactive = newValue?.withRenderingMode(.alwaysTemplate)
        }
    }
    
    @IBInspectable public var colorActive: UIColor?{
        get{
            return self.internalColorActive
        }
        set{
            self.internalColorActive = newValue
            self.updateDots()
        }
    }
    
    @IBInspectable public var colorInactive: UIColor?{
        get{
            return self.internalColorInactive
        }
        set{
            self.internalColorInactive = newValue
            self.updateDots()
        }
    }
    
    public override var currentPage: Int{
        didSet{
            self.updateDots()
        }
    }
    
    private var internalColorActive     : UIColor?
    private var internalColorInactive   : UIColor?
    private var internalIconActive      : UIImage?
    private var internalIconInactive    : UIImage?
    
    func updateDots() {
        
        for (index, view) in self.subviews.enumerated() {
            
            if let imageView = self.imageForSubview(view) {
                imageView.image     = index == self.currentPage ? self.internalIconActive : self.internalIconInactive
                imageView.tintColor = index == self.currentPage ? self.internalColorActive : self.internalColorInactive
            } else {
                self.addDotInView(view, toIndex: index)
            }
        }
    }
    
    private func addDotInView(_ view: UIView, toIndex index: Int) {
        
        let dotImage    = index == self.currentPage ? self.internalIconActive?.withRenderingMode(.alwaysTemplate) : self.internalIconInactive?.withRenderingMode(.alwaysTemplate)
        let img         = UIImageView(image:dotImage)
        img.tintColor   = index == self.currentPage ? self.internalColorActive : self.internalColorInactive
        img.center      = CGPoint(x: view.frame.height/2, y: view.frame.width/2)
        
        view.layer.cornerRadius = 0
        view.clipsToBounds      = false
        view.backgroundColor    = .clear
        view.addSubview(img)
    }
    
    private func imageForSubview(_ view:UIView) -> UIImageView? {
        
        return view.subviews.filter({ $0 is UIImageView}).first as? UIImageView
    }
    
    open override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        self.updateDots()
    }
}
