//
//  CSUISlider.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit
import CSUtilities

@IBDesignable open class CSUISlider: CSUIShadowSlider {
    
    @IBInspectable public var allowThumbTinColor: Bool{
        get{
            return self.internalAllowThumbTinColor
        }
        set{
            self.internalAllowThumbTinColor = newValue
            self.updateThumbImage()
        }
    }
    
    @IBInspectable public var thumbImage : UIImage?{
        get{
            return self.currentThumbImage
        }set{
            self.internalThumbImage = newValue
            self.updateThumbImage()
        }
    }
    
    @IBInspectable public var thumbColor: UIColor {
        get {
            return self.internalThumColor
        }
        set(newValue) {
            self.internalThumColor = newValue
            self.updateThumbImage()
        }
    }

    private var internalAllowThumbTinColor = false
    private var internalThumbImage : UIImage?
    private var internalThumColor = UIColor.black
    
    private func updateThumbImage() {
        
        var image = self.internalThumbImage
        
        if self.allowThumbTinColor {
            image = image?.withRenderingMode(.alwaysTemplate).tintWithColor(self.internalThumColor)
        }
        
        self.setThumbImage(image, for: .normal)
        self.setThumbImage(image, for: .highlighted)
    }
}
