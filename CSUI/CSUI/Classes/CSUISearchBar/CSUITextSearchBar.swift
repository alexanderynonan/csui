//
//  CSUITextSearchBar.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable open class CSUITextSearchBar: UISearchBar {
    
    @IBInspectable public var textColor: UIColor?{
        get{
            let textField = self.value(forKey: "searchField") as? UITextField
            return textField?.textColor
        }
        set{
            let textField = self.value(forKey: "searchField") as? UITextField
            textField?.textColor = newValue
        }
    }
    
    @IBInspectable public var placeHolderColor: UIColor?{
        get{
            let textField = self.value(forKey: "searchField") as? UITextField
            let textFieldLabel = textField?.value(forKey: "placeholderLabel") as? UILabel
            return textFieldLabel?.textColor
        }
        set{
            let textField = self.value(forKey: "searchField") as? UITextField
            let textFieldLabel = textField?.value(forKey: "placeholderLabel") as? UILabel
            textFieldLabel?.textColor = newValue
        }
    }
    
    public var font: UIFont?{
        get{
            let textField = self.value(forKey: "searchField") as? UITextField
            return textField?.font
        }
        set{
            let textField = self.value(forKey: "searchField") as? UITextField
            textField?.font = newValue
        }
    }
}
