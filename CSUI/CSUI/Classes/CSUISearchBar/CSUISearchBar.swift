//
//  CSUISearchBar.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit


@IBDesignable open class CSUISearchBar: CSUIShadowSearchBar {
    
    @IBInspectable public var iconSearch: UIImage?{
        get{
            let textField = self.value(forKey: "searchField") as? UITextField
            return (textField?.leftView as? UIImageView)?.image
        }
        set{
            let textField = self.value(forKey: "searchField") as? UITextField
            (textField?.leftView as? UIImageView)?.image = newValue
        }
    }
    
    @IBInspectable public var iconClear: UIImage?{
        get{
            let textField = self.value(forKey: "searchField") as? UITextField
            let clearButton = textField?.value(forKey: "clearButton") as? UIButton
            return clearButton?.imageView?.image
        }
        set{
            let textField = self.value(forKey: "searchField") as? UITextField
            let clearButton = textField?.value(forKey: "clearButton") as? UIButton
            clearButton?.setImage(newValue, for: .normal)
        }
    }
}
