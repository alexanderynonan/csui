//
//  CSUIRestrictTextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit
import CSUtilities

@IBDesignable public class CSUIRestrictTextField: CSUIIconTextField {
    
    @IBInspectable public var restrictInput         : Bool = false
    @IBInspectable public var replaceSpecialText    : Bool = false
    @IBInspectable public var allowNumbers          : Bool = false
    @IBInspectable public var allowAlphabetic       : Bool = false
    @IBInspectable public var allowEmail            : Bool = false
    @IBInspectable public var maxLength             : Int = 250
    
    public override func awakeFromNib() {
        
        super.awakeFromNib()
        self.addTarget(self, action: #selector(self.changeTextWhenUseIsTyping(_:)), for: .editingChanged)
        self.addTarget(self, action: #selector(self.changeTextToMaxLength(_:)), for: .editingChanged)
    }
    
    @objc private func changeTextToMaxLength(_ sender: UITextField){
        
        var result = self.text
        
        if result?.count ?? 0 > self.maxLength{
            let range = self.maxLength...((result?.count ?? self.maxLength) - 1)
            result = result?.replaceByString("", inRange: range)
        }
        
        sender.text = result
    }
    
    @objc private func changeTextWhenUseIsTyping(_ sender: UITextField){
        
        if self.restrictInput {
            
            let textAllowed     = self.allowNumbers ? "0-9" : ""
            let textAlphabetic  = self.allowAlphabetic ? "A-Za-zÑñÁáÉéÍíÓóÚú " : ""
            let textEmail       = self.allowEmail ? "@.-_A-Za-z0-9" : ""
            var result          = self.text?.replacingOccurrences( of:"[^\(textAllowed + textAlphabetic + textEmail)]", with: "", options: .regularExpression)
            
            if self.replaceSpecialText {
                result = result?.replaceSpecialCharacters() ?? ""
            }

            sender.text = result
        }
    }
}
