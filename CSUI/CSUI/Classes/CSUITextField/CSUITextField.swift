//
//  CSUITextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@objc public protocol CSUITextFieldDelegate {
    
    @objc optional func textFieldDeleteBackward(_ textField: CSUITextField)
    @objc optional func textFieldUserDidEndEditing(_ textField: CSUITextField)
    @objc optional func textField(_ textField: CSUITextField, tapInLeftIconButton button: UIButton)
    @objc optional func textField(_ textField: CSUITextField, tapInRightIconButton button: UIButton)
}

@IBDesignable public class CSUITextField: CSUITextFieldSafeKeyboard {
    
    @IBOutlet weak open var textFieldDelegate   : CSUITextFieldDelegate?
    
    private var typingTimer                     : DispatchSourceTimer?
    
    public override var text: String?{
        didSet{
             self.startTimeOutTyping()
        }
    }
    
    private func startTimeOutTyping() {
        
        self.cancelTimerKeyboard()
        self.typingTimer = DispatchSource.makeTimerSource()
        self.typingTimer?.schedule(deadline: .now() + 0.5, repeating: 1)
        self.typingTimer?.setEventHandler(handler: {
            
            DispatchQueue.main.async {
                self.cancelTimerKeyboard()
                self.textFieldDelegate?.textFieldUserDidEndEditing?(self)
            }
        })

        self.typingTimer?.activate()
    }
    
    private func cancelTimerKeyboard(){
        
        self.typingTimer?.cancel()
        self.typingTimer = nil
    }
    
    public override func deleteBackward() {
        
        super.deleteBackward()
        self.textFieldDelegate?.textFieldDeleteBackward?(self)
    }
}
