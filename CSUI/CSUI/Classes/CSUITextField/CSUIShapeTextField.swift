//
//  CSUIShapeTextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUIShapeTextField: UITextField {
    
    @IBInspectable public var underline : Bool {
        get{
            return self.internalUnderline
        }
        set(newValue){
            self.internalUnderline = newValue
            self.updateBorders()
        }
    }
    
    @IBInspectable public var borderColor : UIColor{
        
        get{
            return self.internalBorderColor
        }
        set(newValue){
            self.internalBorderColor = newValue
            self.updateBorders()
        }
    }
    
    @IBInspectable public var borderWidth : CGFloat{
        get{
            return self.layer.borderWidth
        }
        set(newValue){
            self.internalBorderWidth = newValue
            self.updateBorders()
        }
    }
    
    @IBInspectable public var cornerRadius : CGFloat{
        get{
            return self.layer.cornerRadius
        }
        set(newValue){
            self.internalCornerRadius = (newValue < 0) ? 0 : newValue
            self.layer.masksToBounds = (self.internalCornerRadius != 0)
            self.updateBorders()
        }
    }
    
    @IBInspectable public var rightTop : Bool{
        get{
            return self.internalRightTop
        }
        set(newValue){
            self.internalRightTop = newValue
            self.setupCorners()
            self.updateBorders()
        }
    }
    
    @IBInspectable public var leftTop : Bool{
        get{
            return self.internalLeftTop
        }
        set(newValue){
            self.internalLeftTop = newValue
            self.setupCorners()
            self.updateBorders()
        }
    }
    
    @IBInspectable public var rightDown : Bool{
        get{
            return self.internalRightDown
        }
        set(newValue){
            self.internalRightDown = newValue
            self.setupCorners()
            self.updateBorders()
        }
    }
    
    @IBInspectable public var leftDown : Bool{
        get{
            return self.internalLeftDown
        }
        set(newValue){
            self.internalLeftDown = newValue
            self.setupCorners()
            self.updateBorders()
        }
    }
    
    private var internalRightTop        = false
    private var internalRightDown       = false
    private var internalLeftTop         = false
    private var internalLeftDown        = false
    private var internalBorderColor     = UIColor.clear
    private var internalBorderWidth     : CGFloat = 0
    private var internalUnderline       = false
    private var internalCornerRadius    : CGFloat = 0
    
    private lazy var viewUnderline : UIView = {
       
        let view = UIView(frame: CGRect(x: 0, y: self.frame.height - self.internalBorderWidth, width: self.frame.width, height: self.internalBorderWidth))
        view.backgroundColor = self.internalBorderColor
        
        return view
    }()
    
    private func setupCorners() {
        
        let maskedCorners           = self.getMaskedCorners()
        self.layer.maskedCorners    = maskedCorners
    }
    
    private func updateBorders() {
        
        self.layer.borderColor  = nil
        self.layer.borderWidth  = .zero
        self.layer.cornerRadius = .zero
        
        if self.internalUnderline {
            self.addSubview(self.viewUnderline)
            self.viewUnderline.frame = CGRect(x: 0, y: self.frame.height - self.internalBorderWidth, width: self.frame.width, height: self.internalBorderWidth)
            self.viewUnderline.backgroundColor = self.internalBorderColor
        } else {
            self.viewUnderline.removeFromSuperview()
            self.layer.borderColor  = self.internalBorderColor.cgColor
            self.layer.borderWidth  = self.internalBorderWidth
            self.layer.cornerRadius = self.internalCornerRadius
        }
    }
    
    public func getMaskedCorners() -> CACornerMask {
        
        var arrayCornerMask = [CACornerMask]()
        
        if self.leftTop     {arrayCornerMask.append(.layerMinXMinYCorner)}
        if self.rightTop    {arrayCornerMask.append(.layerMaxXMinYCorner)}
        if self.leftDown    {arrayCornerMask.append(.layerMinXMaxYCorner)}
        if self.rightDown   {arrayCornerMask.append(.layerMaxXMaxYCorner)}
        
        return arrayCornerMask.count != 0 ? CACornerMask(arrayCornerMask) : [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.updateBorders()
    }
}
