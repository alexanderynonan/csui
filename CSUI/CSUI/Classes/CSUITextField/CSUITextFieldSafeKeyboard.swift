//
//  CSUITextFieldSafeKeyboard.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUITextFieldSafeKeyboard: CSUIErrorTextField {
    
    @IBInspectable public var safeEnabled : Bool{
        get{
            return self.internalSafeEnabled
        }set{
            self.internalSafeEnabled = newValue
            self.inputViewController = newValue ? self.getSafeKeyboard() : nil
        }
    }
    
    @IBInspectable public var storyboardName : String{
        get{
            return self.internalStoryboardName
        }
        set{
            self.internalStoryboardName = newValue
            self.inputViewController = self.safeEnabled ? self.getSafeKeyboard() : nil
        }
    }
    
    @IBInspectable public var controllerIdentifier : String{
        get{
            return self.internalControllerIdentifier
        }
        set{
            self.internalControllerIdentifier = newValue
            self.inputViewController = self.safeEnabled ? self.getSafeKeyboard() : nil
        }
    }
    
    private var internalStoryboardName          = ""
    private var internalControllerIdentifier    = ""
    private var internalSafeEnabled             = false
    private var _inputViewController            : UIInputViewController?
    
    override public var inputViewController: UIInputViewController?{
        get { return _inputViewController }
        set { _inputViewController = newValue }
    }
    
    private func getSafeKeyboard() -> UIInputViewController? {
                
        if self.storyboardName.count == 0 {return CSUISafeKeyboard()}
        if self.controllerIdentifier.count == 0 {return CSUISafeKeyboard()}
        
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: self.controllerIdentifier) as? UIInputViewController ?? CSUISafeKeyboard()
    }
}
