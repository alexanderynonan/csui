//
//  CSUIErrorTextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit
import CSUtilities

@IBDesignable public class CSUIErrorTextField: CSUIPlaceholderTextField {
    
    @IBOutlet public weak var bottomConstraint  : NSLayoutConstraint?
    
    private var topLabelErrorConstraint         : NSLayoutConstraint?
    private var initialBottonConstraintConstant : CGFloat = 0
    
    private var errorMessage: String? {
        didSet{
            self.lblErrorMessage.alpha  = 1
            self.lblErrorMessage.text   = self.errorMessage
            self.stateContent = .error
        }
    }
    
    private var sizeToErrorMessage : CGSize = .zero
    public lazy var lblErrorMessage: UILabel = {
       
        let lbl             = UILabel()
        lbl.text            = self.errorMessage
        lbl.font            = UIFont(name: self.font?.fontName ?? "", size: 11)
        lbl.textColor       = self.p_errorColor
        lbl.alpha           = 0
        lbl.numberOfLines   = 0
        lbl.lineBreakMode   = .byWordWrapping
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    public override var text: String?{
        didSet{
            self.hideErrorMessage()
        }
    }
    
    public override func awakeFromNib() {
        
        super.awakeFromNib()
        self.addConstraintsToLabelError()
    }
    
    public func showErrorMessageWithText(_ errorMessage: String) {
        
        self.errorMessage = errorMessage
        self.sizeToErrorMessage = self.lblErrorMessage.attributedText?.getSizeToWidth(self.lblErrorMessage.frame.width) ?? .zero
        
        UIView.animate(withDuration: 0.3) {
            self.topLabelErrorConstraint?.constant = 5
            self.bottomConstraint?.constant = self.initialBottonConstraintConstant + self.sizeToErrorMessage.height + (self.topLabelErrorConstraint?.constant ?? 0)
            self.getSuperView(self).layoutIfNeeded()
        }
    }
    
    public func hideErrorMessage() {
        
        UIView.animate(withDuration: 0.3) {
            self.topLabelErrorConstraint?.constant  = -self.sizeToErrorMessage.height
            self.lblErrorMessage.alpha              = 0
            self.bottomConstraint?.constant         = self.initialBottonConstraintConstant
            self.stateContent = (self.text?.count == 0 && !self.isFirstResponder) ? .normal : .editing
            self.getSuperView(self).layoutIfNeeded()
        }
    }
    
    private func getSuperView(_ view: UIView) -> UIView {
        
        if let newSuperView = view.superview {
            return self.getSuperView(newSuperView)
        }
        
        return view
    }
    
    private func addConstraintsToLabelError(){
        
        if let bottomConstraint = self.bottomConstraint, self.initialBottonConstraintConstant == 0 {
            
            self.initialBottonConstraintConstant = bottomConstraint.constant
            self.superview?.insertSubview(self.lblErrorMessage, belowSubview: self)
            
            self.topLabelErrorConstraint = self.lblErrorMessage.topAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
            self.topLabelErrorConstraint?.isActive = true
            self.lblErrorMessage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
            self.lblErrorMessage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
            self.getSuperView(self).layoutIfNeeded()
        }
    }
}
