//
//  CSUIPlaceholderTextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

public enum CSUIPlaceholderTextFieldState: Int{
    
    case normal = 0
    case editing
    case endEditing
    case error
}

@IBDesignable public class CSUIPlaceholderTextField: CSUIRestrictTextField {
    
    @IBInspectable public var scaleEnabled      : Bool      = false
    @IBInspectable public var minimunScale      : CGFloat   = 1
    @IBInspectable public var p_errorColor      : UIColor   = .black
    @IBInspectable public var p_editingColor    : UIColor   = .black
    @IBInspectable public var p_endEditingColor : UIColor   = .black
    
    @IBInspectable public var p_holderColor     : UIColor{
        get{
            return self.internalPlaceholderColor
        }
        set{
            self.internalPlaceholderColor = newValue
            self.changePlaceHolderColor(newValue)
        }
    }
    
    public override var text: String?{
        didSet{
            
            if (self.text?.count ?? 0) > 0, self.isFirstResponder{
                self.stateContent = .editing
                
            }else if self.text?.count == 0 && !self.isFirstResponder && self.stateContent != .editing{
                self.updateLayoutToStateContent(.normal, didEndEditing: true)
                self.stateContent = .normal
            }
        }
    }
    
    public override var placeholder: String? {
        didSet{
            self.lblPlaceHolder.text = self.placeholder?.count == 0 ? self.lblPlaceHolder.text : self.placeholder
        }
    }
    
    private var internalPlaceholderColor        = UIColor.black
    public var stateContent                     = CSUIPlaceholderTextFieldState.normal{
        didSet{
            let colors = self.getColorState()
            self.changePlaceHolderColor(colors.placeHolderColor)
            self.layer.borderColor = colors.borderColor.cgColor
        }
    }
    
    lazy private var lblPlaceHolder : UILabel = {

        let lbl = UILabel()
        lbl.attributedText = self.attributedPlaceholder
        lbl.backgroundColor = .white
        lbl.sizeToFit()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    public override func awakeFromNib() {
        
        super.awakeFromNib()
        self.addTarget(self, action: #selector(self.editingDidBegin(_:)),   for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.editingDidEnd(_:)),     for: .editingDidEnd)
    }
    
    private func updateLayoutToStateContent(_ stateContent: CSUIPlaceholderTextFieldState, didEndEditing didEditing: Bool) {
        
        if didEditing && self.text?.count == 0 && stateContent != .error {
            self.stateContent = stateContent
            self.animatePlaceHolderToDown()
            
        }else if !didEditing && self.text?.count == 0{
            self.stateContent = stateContent
            self.animatePlaceHolderToUp()
            
        }else if didEditing && (self.text?.count ?? 0) > 0 && stateContent != .error {
            self.stateContent = stateContent
            
        }else if !didEditing {
            self.stateContent = stateContent
        }
    }
}

extension CSUIPlaceholderTextField {
 
    //MARK: - Keyboard events
    
    @objc private func editingDidEnd(_ sender: UITextField?) {
        
        if self.scaleEnabled, self.stateContent != .error{
            self.updateLayoutToStateContent((self.text?.count ?? 0) == 0 ? .normal : .endEditing, didEndEditing: true)
        }else if !self.scaleEnabled, self.stateContent != .error {
            self.stateContent = (self.text?.count ?? 0) == 0 ? .normal : .endEditing
        }
    }

    @objc private func editingDidBegin(_ sender: UITextField) {
    
        if self.scaleEnabled {
            
            self.superview?.insertSubview(self.lblPlaceHolder, aboveSubview: self)
            self.addConstraintsToPlaceHolder()
            self.updateLayoutToStateContent(.editing, didEndEditing: false)

        }else {
            self.stateContent = self.stateContent == .error ? .error : .editing
            self.lblPlaceHolder.removeFromSuperview()
        }
    }
    
    private func animatePlaceHolderToDown(){
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            
            self.lblPlaceHolder.transform   = .identity
            
        }) { (success) in
            if !self.isEditing{
                self.attributedPlaceholder      = self.lblPlaceHolder.attributedText
            }
        }
    }
    
    private func animatePlaceHolderToUp(){
        
        let deltaY = self.frame.height / 2
        let newWidth = self.lblPlaceHolder.frame.width * (1 - self.minimunScale)
        let distance = newWidth / 2 + self.leftMargin - 10
        self.placeholder = ""

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.66, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.lblPlaceHolder.transform   = CGAffineTransform(translationX: -distance, y: -deltaY).scaledBy(x: self.minimunScale, y: self.minimunScale)
        }, completion: nil)
    }
}

extension CSUIPlaceholderTextField {
    
    //MARK: - HelperMethods
    
    private func changePlaceHolderColor(_ newValue: UIColor) {
        
        self.lblPlaceHolder.textColor = newValue
        guard let oldAttributes = self.attributedPlaceholder else { return }
        let range = NSRange(location: 0, length: oldAttributes.length)
        let newAttributes = NSMutableAttributedString(attributedString: oldAttributes)
        newAttributes.addAttribute(NSAttributedString.Key.foregroundColor, value: newValue, range: range)
        self.attributedPlaceholder = newAttributes
    }
    
    private func getColorState() -> (borderColor: UIColor, placeHolderColor: UIColor){

        if self.stateContent == .normal {
            return (self.borderColor, self.p_holderColor)
            
        }else if self.stateContent == .editing, (self.text?.count ?? 0) >= 0, !self.scaleEnabled {
            return (self.p_editingColor, self.p_holderColor)
            
        }else if self.stateContent == .editing, self.scaleEnabled {
            return (self.p_editingColor, self.p_editingColor)
            
        }else if self.stateContent == .endEditing {
            return (self.p_endEditingColor, self.p_endEditingColor)
            
        }else if self.stateContent == .error {
            return (self.p_errorColor, self.p_errorColor)
        }
        
        return (self.borderColor, self.p_holderColor)
    }
    
    private func addConstraintsToPlaceHolder() {
        
        self.lblPlaceHolder.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.lblPlaceHolder.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.leftMargin).isActive = true
        self.superview?.layoutIfNeeded()
    }
}
