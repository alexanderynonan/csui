//
//  CSUIIconTextField.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUIIconTextField: CSUIMarginTextField {
    
    @IBInspectable public var imageLeft : UIImage?{
        get{
            return self.internalImageLeft
        }
        set(newValue){
            
            self.internalImageLeft = newValue
            if let image = newValue{
                self.leftView = self.createButtonWithImage(image, withColor: self.colorLeft, enabled: self.leftEnabled)
                self.leftViewMode = .always
                self.leftMargin = self.leftView?.frame.width ?? 0
            }else{
                self.leftView = nil
                self.leftViewMode = .never
                self.leftMargin = 0.0
            }
        }
    }
    
    @IBInspectable public var imageRight : UIImage?{
        get{
            return self.internalImageRight
        }
        set(newValue){
            
            self.internalImageRight = newValue
            if let image = newValue{
                self.rightView = self.createButtonWithImage(image, withColor: self.colorRight, enabled: self.rightEnabled)
                self.rightViewMode = .always
                self.rightMargin = self.rightView?.frame.width ?? 0
            }else{
                self.rightView = nil
                self.rightViewMode = .never
                self.rightMargin = 0.0
            }
        }
    }
    
    @IBInspectable public var colorLeft : UIColor{
        get{
            return self.internalColorLeft
        }
        set(newValue){
            self.internalColorLeft = newValue
            (self.leftView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.tintColor = newValue
        }
    }
    
    @IBInspectable public var colorRight : UIColor{
        get{
            return self.internalColorRight
        }
        set(newValue){
            self.internalColorRight = newValue
            (self.rightView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.tintColor = newValue
        }
    }
    
    @IBInspectable public var leftEnabled : Bool{
        get{
            return (self.leftView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.isUserInteractionEnabled ?? false
        }
        set{
            (self.leftView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.isUserInteractionEnabled = newValue
        }
    }
    
    @IBInspectable public var rightEnabled : Bool{
        get{
            return (self.rightView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.isUserInteractionEnabled ?? false
        }
        set{
            (self.rightView?.subviews.filter({$0 is UIButton}).first as? UIButton)?.isUserInteractionEnabled = newValue
        }
    }
    
    private var internalColorLeft      : UIColor = .systemBlue
    private var internalColorRight     : UIColor = .systemBlue
    private var internalImageLeft      : UIImage?
    private var internalImageRight     : UIImage?
    
    private func createButtonWithImage(_ image: UIImage, withColor color: UIColor, enabled: Bool) -> UIView {
        
        let contentView = UIView(frame:CGRect(x: 0, y: 0, width: 44, height: self.frame.height))
        
        let btn = UIButton(type: .system)
        btn.setImage(image, for: .normal)
        btn.frame = contentView.bounds
        btn.tintColor = color
        btn.addTarget(self, action: #selector(self.clickBtnIcon(_:)), for: .touchUpInside)
        btn.isUserInteractionEnabled = enabled
        
        contentView.addSubview(btn)
        return contentView
    }
    
    @objc private func clickBtnIcon(_ sender: UIButton) {
        
        if sender.superview == self.leftView, let txt = self as? CSUITextField {
            txt.textFieldDelegate?.textField?(txt, tapInLeftIconButton: sender)
            
        }else if sender.superview == self.rightView, let txt = self as? CSUITextField {
            txt.textFieldDelegate?.textField?(txt, tapInRightIconButton: sender)
        }
    }
}
