//
//  CSUISafeKeyboardKeyShadow.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUISafeKeyboardKeyShadow: CSUISafeKeyboardKeyShape {
    
    @IBInspectable public var shadowColor : UIColor{
        get{
            self.internalShadowColor
        }
        set(newValue){
            self.internalShadowColor = newValue
        }
    }

    @IBInspectable public var shadowOffset : CGSize{
        get{
            return self.internalShadowOffset
        }
        set(newValue){
            self.internalShadowOffset = newValue
        }
    }

    @IBInspectable public var shadowRadius : CGFloat{
        get{
            return self.internalShadowRadius
        }
        set(newValue){
            self.internalShadowRadius = newValue
        }
    }

    @IBInspectable public var shadowOpacity : Float{
        get{
            return self.internalShadowOpacity
        }
        set(newValue){
            self.internalShadowOpacity = newValue
        }
    }
    
    private var internalShadowColor         = UIColor.black
    private var internalShadowOffset        = CGSize(width: 0, height: 1)
    private var internalShadowRadius        : CGFloat = 1
    private var internalShadowOpacity       : Float = 0.3
}
