//
//  CSUISafeKeyboardShape.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUISafeKeyboardKeyShape: CSUISafeKeyboardInsets {
    
    @IBInspectable public var borderColor : UIColor{
        
        get{
            return self.internalBorderColor
        }
        set(newValue){
            self.internalBorderColor = newValue
        }
    }
    
    @IBInspectable public var borderWidth : CGFloat{
        get{
            return self.internalBorderWidth
        }
        set(newValue){
            self.internalBorderWidth = newValue
        }
    }
    
    @IBInspectable public var cornerRadius : CGFloat{
        get{
            return self.internalCornerRadius
        }
        set(newValue){
            self.internalCornerRadius = newValue
        }
    }
    
    private var internalBorderColor         = UIColor.black
    private var internalBorderWidth         : CGFloat = 0
    private var internalCornerRadius        : CGFloat = 5
}
