//
//  CSUISafeKeyboard.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit


public class CSUISafeKeyboard: UIInputViewController {

    override public func viewDidLoad() {
            
        super.viewDidLoad()
    
        if let safekeyboard = self.inputView as? CSUISafeKeyboardView {
            safekeyboard.delegate = self
        }else{
            self.inputView = CSUISafeKeyboardView()
            (self.inputView as? CSUISafeKeyboardView)?.delegate = self
        }
        
        self.inputView?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override public func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        (self.inputView as? CSUISafeKeyboardView)?.shuffledValues()
    }
}

extension CSUISafeKeyboard: CSUISafeKeyboardViewDelegate {
    
    public func safeKeyboardView(_ keyboard: CSUISafeKeyboardView, tapInKeyWithValue value: String) {
        self.textDocumentProxy.insertText(value)
    }
    
    public func safeKeyboardViewPressButtonDeleteBackward(_ keyboard: CSUISafeKeyboardView) {
        self.textDocumentProxy.deleteBackward()
    }
}
