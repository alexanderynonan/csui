//
//  CSUISafeKeyboardKeyStyle.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable public class CSUISafeKeyboardKeyStyle: CSUISafeKeyboardKeyShadow {
    
    @IBInspectable public var keyBackgroundColor : UIColor{
        
        get{
            return self.internalKeyBackgroundColor
        }
        set(newValue){
            self.internalKeyBackgroundColor = newValue
        }
    }
    
    @IBInspectable public var textColor : UIColor{
        
        get{
            return self.internaltextColor
        }
        set(newValue){
            self.internaltextColor = newValue
        }
    }
    
    @IBInspectable public var fontSize : CGFloat {
        
        get{
            return self.internalFontSize
        }
        set(newValue){
            self.internalFontSize = newValue
        }
    }
    
    @IBInspectable public var typeFont : Int {
        
        get{
            return self.internalTypeFont
        }
        set(newValue){
            self.internalTypeFont = newValue
        }
    }
    
    private var internaltextColor           = UIColor.black
    private var internalBackgroundColor     = UIColor.white
    private var internalKeyBackgroundColor  = UIColor.white
    private var internalFontSize            : CGFloat = 24
    private var internalTypeFont            : Int = 3
    
    public func getFontToKeys() -> UIFont{
        
        switch self.typeFont {
        case 0:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.ultraLight)
        case 1:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.thin)
        case 2:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.light)
        case 3:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.regular)
        case 4:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.medium)
        case 5:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.semibold)
        case 6:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.bold)
        case 7:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.heavy)
        case 8:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.black)
        default:
            return UIFont.systemFont(ofSize: self.fontSize, weight: UIFont.Weight.regular)
        }
    }
}
