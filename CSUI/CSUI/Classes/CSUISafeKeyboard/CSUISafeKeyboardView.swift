//
//  CSUISafeKeyboardView.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

public protocol CSUISafeKeyboardViewDelegate {
    func safeKeyboardView(_ keyboard: CSUISafeKeyboardView, tapInKeyWithValue value: String)
    func safeKeyboardViewPressButtonDeleteBackward(_ keyboard: CSUISafeKeyboardView)
}

@IBDesignable public class CSUISafeKeyboardView: CSUISafeKeyboardKeyStyle {

    @IBInspectable public var backIcon: UIImage?
    @IBInspectable public var backColor: UIColor = .black
    
    public var delegate : CSUISafeKeyboardViewDelegate?
    
    lazy var arrayKeyButtons : [CSUIButton] = {
        
        let randomArray = Array(0...9).shuffled()
        
        var arrayKeyButtons = [CSUIButton]()
        for value in randomArray {
            arrayKeyButtons.append(self.drawButtonWithValue(value, inView: self))
        }
        
        return arrayKeyButtons
    }()
    
    private lazy var btnBack: CSUIButton = {
        
        let btnBack = CSUIButton(type: .system)
        btnBack.backgroundColor  = .clear
        btnBack.setImage(self.getIconBackward(), for: .normal)
        btnBack.tintColor = self.backColor
        btnBack.translatesAutoresizingMaskIntoConstraints = false
        btnBack.addTarget(self, action: #selector(self.tapKeyBack(_:)), for: .touchUpInside)
        return btnBack
    }()
    
    private func getIconBackward() -> UIImage? {
        
        guard let icon = self.backIcon else {
            if #available(iOS 13.0, *){
                return UIImage(systemName: "delete.left")
            }
            
            return nil
        }
        
        return icon
    }
    
    public func shuffledValues(){
        
        let randomArray = Array(0...9).shuffled()
        for (index, btn) in self.arrayKeyButtons.enumerated() {
            btn.setTitle("\(randomArray[index])", for: .normal)
        }
    }
    
    private func drawButtonWithValue(_ value: Any, inView view: UIView) -> CSUIButton {

        let btnKey = CSUIButton(type: .system)
        btnKey.backgroundColor  = self.keyBackgroundColor
        btnKey.shadowColor      = self.shadowColor
        btnKey.shadowOffset     = self.shadowOffset
        btnKey.shadowRadius     = self.shadowRadius
        btnKey.shadowOpacity    = self.shadowOpacity
        btnKey.borderColor      = self.borderColor
        btnKey.borderWidth      = self.borderWidth
        btnKey.cornerRadius     = self.cornerRadius
        btnKey.titleLabel?.font = self.getFontToKeys()
        
        btnKey.setTitle("\(value)", for: .normal)
        btnKey.setTitleColor(self.textColor, for: .normal)
        btnKey.translatesAutoresizingMaskIntoConstraints = false
        btnKey.addTarget(self, action: #selector(self.tapKeyInKeyboard(_:)), for: .touchUpInside)
        view.addSubview(btnKey)
        
        return btnKey
    }
    
    @objc private func tapKeyBack(_ btn: CSUIButton) {
        self.delegate?.safeKeyboardViewPressButtonDeleteBackward(self)
    }
    
    @objc private func tapKeyInKeyboard(_ btn: CSUIButton) {
        self.delegate?.safeKeyboardView(self, tapInKeyWithValue: btn.titleLabel?.text ?? "")
    }
    
    private func addHorizontalConstraints(){
        
        for (index, btn) in self.arrayKeyButtons.enumerated(){
            
            if index > 0 {
                btn.widthAnchor.constraint(equalTo: self.arrayKeyButtons[index - 1].widthAnchor).isActive = true
            }
            
            if index < self.arrayKeyButtons.count - 1 {
                
                let rest = index % 3
                
                if rest == 0 {
                    btn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.edgeMargin.left).isActive = true
                }
                
                if rest == 1 || rest == 2{
                    btn.leadingAnchor.constraint(equalTo: self.arrayKeyButtons[index - 1].trailingAnchor, constant: self.rowSpacing).isActive = true
                }
                
                if rest == 2 {
                    btn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -self.edgeMargin.right).isActive = true
                }
            }else{
                btn.centerXAnchor.constraint(equalToSystemSpacingAfter: self.centerXAnchor, multiplier: 0).isActive = true
                self.btnBack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -self.edgeMargin.right).isActive = true
                self.btnBack.widthAnchor.constraint(equalTo: btn.widthAnchor).isActive = true
                self.btnBack.topAnchor.constraint(equalTo: btn.topAnchor).isActive = true
                self.btnBack.heightAnchor.constraint(equalTo: btn.heightAnchor).isActive = true
            }
        }
        
    }
    
    private func addVerticalConstraints(){
        
        for (index, btn) in self.arrayKeyButtons.enumerated(){
            
            if index > 0 {
                btn.heightAnchor.constraint(equalTo: self.arrayKeyButtons[index - 1].heightAnchor).isActive = true
            }
        
            if index < 3{
                btn.topAnchor.constraint(equalTo: self.topAnchor, constant: self.edgeMargin.top).isActive = true
            }else {
                btn.topAnchor.constraint(equalTo: self.arrayKeyButtons[index - 3].bottomAnchor, constant: self.lineSpacing).isActive = true
            }
        }
        
        let safeArea = self.superview?.safeAreaInsets.bottom ?? 0
        self.arrayKeyButtons.last?.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -(self.edgeMargin.bottom + safeArea)).isActive = true
    }
    
    override public func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        self.addSubview(self.btnBack)
        self.addHorizontalConstraints()
        self.addVerticalConstraints()
    }
    
    override public func prepareForInterfaceBuilder() {
    
        super.prepareForInterfaceBuilder()
    }
    
    public func updateHorizontalConstraints(){
        
        for (index, btn) in self.arrayKeyButtons.enumerated(){
            
            if index > 0 {
                btn.widthAnchor.constraint(equalTo: self.arrayKeyButtons[index - 1].widthAnchor).isActive = true
            }
            
            if index < self.arrayKeyButtons.count - 1 {
                
                let rest = index % 3
                
                if rest == 0 {
                    btn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.edgeMargin.left).isActive = true
                }
                
                if rest == 1 || rest == 2{
                    btn.leadingAnchor.constraint(equalTo: self.arrayKeyButtons[index - 1].trailingAnchor, constant: self.rowSpacing).isActive = true
                }
                
                if rest == 2 {
                    btn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -self.edgeMargin.right).isActive = true
                }
            }else{
                btn.centerXAnchor.constraint(equalToSystemSpacingAfter: self.centerXAnchor, multiplier: 0).isActive = true
                self.btnBack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -self.edgeMargin.right).isActive = true
                self.btnBack.widthAnchor.constraint(equalTo: btn.widthAnchor).isActive = true
                self.btnBack.topAnchor.constraint(equalTo: btn.topAnchor).isActive = true
                self.btnBack.heightAnchor.constraint(equalTo: btn.heightAnchor).isActive = true
            }
        }
        
    }

}
