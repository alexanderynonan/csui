//
//  CSUISafeKeyboardInsets.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@IBDesignable public class CSUISafeKeyboardInsets: UIInputView {

    @IBInspectable public var topMargin : CGFloat{
        get{
            return self.edgeMargin.top
        }
        set(newValue){
            self.edgeMargin.top = newValue
        }
    }
    
    @IBInspectable public var leftMargin : CGFloat{
        get{
            return self.edgeMargin.left
        }
        set(newValue){
            self.edgeMargin.left = newValue
        }
    }
    
    @IBInspectable public var rightMargin : CGFloat{
        get{
            return self.edgeMargin.right
        }
        set(newValue){
            self.edgeMargin.right = newValue
        }
    }
    
    @IBInspectable public var bottomMargin : CGFloat{
        get{
            return self.edgeMargin.bottom
        }
        set(newValue){
            self.edgeMargin.bottom = newValue
        }
    }
    
    @IBInspectable public var lineSpacing : CGFloat{
        get{
            return self.internalLineSpacing
        }
        set(newValue){
            self.internalLineSpacing = newValue
        }
    }
    
    @IBInspectable public var rowSpacing : CGFloat{
        get{
            return self.internalRowSpacing
        }
        set(newValue){
            self.internalRowSpacing = newValue
        }
    }
    
    public  var edgeMargin                  : UIEdgeInsets = UIEdgeInsets(top: 6.0, left: 5.5, bottom: (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) == 0 ? 3 : 44, right: 5.5)
    private var internalLineSpacing         : CGFloat = 7
    private var internalRowSpacing          : CGFloat = 6
}
