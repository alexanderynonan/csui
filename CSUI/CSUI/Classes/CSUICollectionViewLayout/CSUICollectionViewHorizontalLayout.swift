//
//  CSUICollectionViewHorizontalLayout.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@objc public protocol CSUICollectionViewHorizontalLayoutDelegate {
    
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, widthForItemAtIndexPath indexPath: IndexPath, toHeightCell heightCell: CGFloat) -> CGFloat
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, paddingForSection section: Int) -> UIEdgeInsets
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, horizontalItemSeparatorForSection section: Int) -> CGFloat
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, verticalItemSeparatorForSection section: Int) -> CGFloat
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, heightForHeaderInSection section: Int) -> CGFloat
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, heightForFooterInSection section: Int) -> CGFloat
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, newSizeForContent size: CGSize)
    @objc optional func collectionViewHorizontalLayout(_ viewLayout: CSUICollectionViewHorizontalLayout, heightToCellForSection section: Int) -> Int
}

@IBDesignable public class CSUICollectionViewHorizontalLayout: UICollectionViewLayout {
    
    @IBOutlet public var delegate  : CSUICollectionViewHorizontalLayoutDelegate?
    
    @IBInspectable public var horizontalSpace : CGFloat{
        get{
            return self.internalHorizontalSpace ?? 0
        }set{
            self.internalHorizontalSpace = newValue
        }
    }
    
    @IBInspectable public var verticalSpace : CGFloat{
        get{
            return self.internalVerticalSpace ?? 0
        }set{
            self.internalVerticalSpace = newValue
        }
    }
    
    @IBInspectable public var heightCell : Int{
        get{
            return self.internalHeightCell ?? 1
        }set{
            self.internalHeightCell = newValue
        }
    }
    
    private var internalHorizontalSpace : CGFloat?
    private var internalVerticalSpace   : CGFloat?
    private var internalHeightCell : Int?
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    private var contentSize             = CGSize.zero
    
    func getIndexWithMinorHeightInArray(_ array: [CGPoint]) -> Int {
        
        let arraySort = array.sorted(by: {$0.y < $1.y})
        guard let first = arraySort.first else { return 0 }
        return array.firstIndex(of: first) ?? 0
    }
    
    func getIndexWithMajorHeightInArray(_ array: [CGPoint]) -> Int {
        
        let arraySort = array.sorted(by: {$0.y > $1.y})
        guard let first = arraySort.first else { return 0 }
        return array.firstIndex(of: first) ?? 0
    }
    
    override public func prepare() {
        
        super.prepare()
        
        self.cache.removeAll()
        guard cache.isEmpty == true,let collectionView = self.collectionView else { return }
        
        var yOffset : CGFloat = 0
        var xOffset : CGFloat = 0
        
        for section in 0..<(collectionView.numberOfSections){
            
            let horizontalSpace = self.internalHorizontalSpace  ?? self.delegate?.collectionViewHorizontalLayout?(self, horizontalItemSeparatorForSection: section) ?? 10
            let verticalSpace   = self.internalVerticalSpace    ?? self.delegate?.collectionViewHorizontalLayout?(self, verticalItemSeparatorForSection: section) ?? 10
            let heightCell      = self.internalHeightCell       ?? self.delegate?.collectionViewHorizontalLayout?(self, heightToCellForSection: section) ?? 1
            let inset           = self.delegate?.collectionViewHorizontalLayout?(self, paddingForSection: section) ?? .zero
            let heightForHeader = self.delegate?.collectionViewHorizontalLayout?(self, heightForHeaderInSection: section) ?? 0
            let heightForFooter = self.delegate?.collectionViewHorizontalLayout?(self, heightForFooterInSection: section) ?? 0
            
            yOffset = yOffset + inset.top + heightForHeader
            xOffset = inset.left
            
            var itemSize = CGSize.zero
            
            for item : Int in 0 ..< collectionView.numberOfItems(inSection: section) {
                
                let indexPath = IndexPath(item: item, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                
                let widthCell = self.delegate?.collectionViewHorizontalLayout?(self, widthForItemAtIndexPath: indexPath, toHeightCell: CGFloat(heightCell)) ?? 0
                itemSize = CGSize(width: Int(widthCell), height: heightCell)
                
                let newWidth = xOffset + itemSize.width
                let availableWidth = collectionView.frame.size.width - inset.right
                
                if newWidth <= availableWidth {
                    attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
                    xOffset += (itemSize.width + horizontalSpace)
                }else{
                    
                    yOffset += (itemSize.height + verticalSpace)
                    xOffset = inset.left
                    attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
                    xOffset += (itemSize.width + horizontalSpace)
                }
            
                self.cache.append(attributes)
            }
            
            yOffset += CGFloat(heightCell) + inset.bottom + heightForFooter
        }
        
        self.contentSize = CGSize(width: collectionView.frame.size.width, height: yOffset)
        self.delegate?.collectionViewHorizontalLayout?(self, newSizeForContent: self.contentSize)
    }
    
    override public var collectionViewContentSize: CGSize {
        return self.contentSize
    }
    
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in self.cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cache[indexPath.item]
    }
}
