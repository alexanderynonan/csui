//
//  CSUICollectionViewVerticalLayout.swift
//  CSUI
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

@objc public protocol CSUICollectionViewVerticalLayoutDelegate {
    
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, heightForItemAtIndexPath indexPath: IndexPath, toWidthCell widthCell: CGFloat) -> CGFloat
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, paddingInSection section: Int) -> UIEdgeInsets
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, horizontalSpaceSeparatorInSection section: Int) -> CGFloat
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, verticalSpaceSeparatorInSection section: Int) -> CGFloat
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, heightForHeaderInSection section: Int) -> CGFloat
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, heightForFooterInSection section: Int) -> CGFloat
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, newSizeForContent size: CGSize)
    @objc optional func collectionViewVerticalLayout(_ viewLayout: CSUICollectionViewVerticalLayout, numberOfColumnsInSection section: Int) -> Int
}

@IBDesignable public class CSUICollectionViewVerticalLayout: UICollectionViewLayout {
    
    @IBOutlet public weak var delegate  : CSUICollectionViewVerticalLayoutDelegate?
        
    @IBInspectable public var horizontalSpace: CGFloat{
        get{
            return self.internalHorizontalSpace ?? 0
        }set{
            self.internalHorizontalSpace = newValue
        }
    }
    
    @IBInspectable public var verticalSpace: CGFloat{
        get{
            return self.internalVerticalSpace ?? 0
        }set{
            self.internalVerticalSpace = newValue
        }
    }
    
    @IBInspectable public var numberOfColumns: Int{
        get{
            return self.internalNumberOfColumns ?? 1
        }set{
            self.internalNumberOfColumns = newValue
        }
    }
    
    private var internalHorizontalSpace : CGFloat?
    private var internalVerticalSpace   : CGFloat?
    private var internalNumberOfColumns : Int?
    
    fileprivate var cache               = [UICollectionViewLayoutAttributes]()
    private var contentSize             = CGSize.zero
    
    private func getIndexWithMinorHeightInArray(_ array: [CGPoint]) -> Int {
        
        let arraySort = array.sorted(by: {$0.y < $1.y})
        guard let first = arraySort.first else { return 0 }
        return array.firstIndex(of: first) ?? 0
    }
    
    private func getIndexWithMajorHeightInArray(_ array: [CGPoint]) -> Int {
        
        let arraySort = array.sorted(by: {$0.y > $1.y})
        guard let first = arraySort.first else { return 0 }
        return array.firstIndex(of: first) ?? 0
    }
    
    override public func prepare() {
        
        super.prepare()
        
        self.cache.removeAll()
        guard cache.isEmpty == true,let collectionView = self.collectionView else { return }
        
        var yOffset : CGFloat = 0
        
        for section in 0..<(collectionView.numberOfSections){
            
            let horizontalSpace = self.internalHorizontalSpace  ?? self.delegate?.collectionViewVerticalLayout?(self, horizontalSpaceSeparatorInSection: section) ?? 10
            let verticalSpace   = self.internalVerticalSpace    ?? self.delegate?.collectionViewVerticalLayout?(self, verticalSpaceSeparatorInSection: section) ?? 10
            let numberOfColumns = self.internalNumberOfColumns  ?? self.delegate?.collectionViewVerticalLayout?(self, numberOfColumnsInSection: section) ?? 1
            let inset           = self.delegate?.collectionViewVerticalLayout?(self, paddingInSection: section) ?? .zero
            let heightForHeader = self.delegate?.collectionViewVerticalLayout?(self, heightForHeaderInSection: section) ?? 0
            let heightForFooter = self.delegate?.collectionViewVerticalLayout?(self, heightForFooterInSection: section) ?? 0
            let columnWidth     = (collectionView.frame.width - inset.left - inset.right - (horizontalSpace * CGFloat(numberOfColumns - 1))) / CGFloat(numberOfColumns)
            
            yOffset = yOffset + inset.top + heightForHeader
            
            var itemSize = CGSize.zero
            var arrayPositions = [CGPoint]()
            
            for column in 0..<numberOfColumns {
                arrayPositions.append(CGPoint(x: inset.left + (columnWidth + horizontalSpace) * CGFloat(column), y: yOffset))
            }
            
            for item : Int in 0 ..< collectionView.numberOfItems(inSection: section) {
                
                let indexPath = IndexPath(item: item, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                
                let heightCell = self.delegate?.collectionViewVerticalLayout?(self, heightForItemAtIndexPath: indexPath, toWidthCell: columnWidth) ?? 0
                itemSize = CGSize(width: columnWidth, height: heightCell)
                
                let currentIndexColumn = self.getIndexWithMinorHeightInArray(arrayPositions)
                attributes.frame = CGRect(x: arrayPositions[currentIndexColumn].x, y: arrayPositions[currentIndexColumn].y, width: itemSize.width, height: itemSize.height).integral
                
                arrayPositions[currentIndexColumn].y = arrayPositions[currentIndexColumn].y + attributes.frame.height + verticalSpace
                
                self.cache.append(attributes)
            }
            
            let columnMajorHeightIndex = self.getIndexWithMajorHeightInArray(arrayPositions)
            
            yOffset = arrayPositions[columnMajorHeightIndex].y + inset.bottom + heightForFooter
        }
        
        self.contentSize = CGSize(width: collectionView.frame.size.width, height: yOffset)
        self.delegate?.collectionViewVerticalLayout?(self, newSizeForContent: self.contentSize)
    }
    
    override public var collectionViewContentSize: CGSize {
        return self.contentSize
    }
    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in self.cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
            
        }
        return visibleLayoutAttributes
    }
    
    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cache[indexPath.item]
    }
}
