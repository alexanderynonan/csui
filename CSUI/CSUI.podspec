Pod::Spec.new do |s|
  s.name                  = 'CSUI'
  s.version               = '1.0.0'
  s.platform              = :ios
  s.ios.deployment_target = '11.0'

  # Files reference
  s.source_files          = 'CSUI/Classes/*/**'

  # Dependency
 
  s.dependency 'CSUtilities' 

  # Podspec description
  s.summary               = 'A short description of CSUI.'
  s.source                = { :git => 'https://alexanderynonan/CSUI.git', :tag => s.version.to_s }
  s.homepage              = 'https://alexanderynonan/CSUI'
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { 'Alexander Ynoñan' => 'alexanderynonan@gmail.com' }
end
