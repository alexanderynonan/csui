//
//  CSUIViewController.swift
//  CSUtilities
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func notificationGesture() -> UINotificationFeedbackGenerator {
        return UINotificationFeedbackGenerator()
    }
    
    public func openWhatssap(number : Any) -> String{
        return "https://api.whatsapp.com/send?phone=+51\(number)&text=Hola.."
    }
    
    public func openPhone(phoneNumber: String?) {
        guard let url = URL(string: "tel://\(phoneNumber?.replacingOccurrences(of: " ", with: "") ?? "")"), UIApplication.shared.canOpenURL(url) else {
            self.showSystemAlertGeneral("El número del contacto es inválido", message: nil, cancel: CSAlertButton(title: "Entiendo"), withCompletion: nil)
            return
        }
        UIApplication.shared.open(url, options: [:]) { success in
            print(success)
        }
    }
    
    public func classNameAsString() -> String {
        return String(describing: type(of: self))
    }
    
    @IBAction public func btnExit(_ sender : Any?){
        self.notificationGesture().notificationOccurred(.warning)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction public func btnTapKeyboard(_ sender : Any?){        
        self.view.endEditing(true)
    }
    
    @IBAction public func btnExitDissmiss(_ sender : Any?){
        self.notificationGesture().notificationOccurred(.warning)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func getAutomaticStatusBarColor() -> UIStatusBarStyle{
        
        let sizeStatus = UIApplication.shared.statusBarFrame.size
        
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: sizeStatus.width, height: sizeStatus.height))
        let img = renderer.image { (context) in
            self.view.layer.render(in: context.cgContext)
        }
        
        let color = self.averageColor(img) ?? UIColor.white
        let middleColor = UIColor(displayP3Red: 0.99, green: 0.99, blue: 0, alpha: 1)
        return color.toHexString() < middleColor.toHexString() ? .lightContent : .default
        
    }
    
    fileprivate func averageColor(_ image: UIImage) -> UIColor? {
        
        guard let img = CIImage(image: image) else { return nil }
        let extentVector = CIVector(x: img.extent.origin.x, y: img.extent.origin.y, z: img.extent.size.width, w: img.extent.size.height)
        
        guard let filter = CIFilter(name: "CIAreaAverage", parameters: [kCIInputImageKey: img, kCIInputExtentKey: extentVector]) else { return nil }
        guard let outputImage = filter.outputImage else { return nil }
        
        var bitmap = [UInt8](repeating: 0, count: 4)
        let context = CIContext(options: [CIContextOption.workingColorSpace: kCFNull!])
        context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: CIFormat.RGBA8, colorSpace: nil)
        
        return UIColor(red: CGFloat(bitmap[0]) / 255, green: CGFloat(bitmap[1]) / 255, blue: CGFloat(bitmap[2]) / 255, alpha: CGFloat(bitmap[3]) / 255)
    }
    
}

public extension UINavigationController {
    
//  ACCEDER A LAS VISTA DE NAVEGACIONES VIEW CONTROLLERS
    func showfirstViewController(to Controller : UIViewController, identifier : String){
        for item in Controller.navigationController?.viewControllers ?? []{
            if item.classNameAsString() == identifier{
                item.navigationController?.popToViewController(item, animated: true)
            }
        }
    }
}

extension Array {
    
//    DELETE DATOS DUPLICADOS
    func filterDuplicate(_ keyValue:((AnyHashable...)->AnyHashable,Element)->AnyHashable) -> [Element]
    {
        func makeHash(_ params:AnyHashable ...) -> AnyHashable
        {
           var hash = Hasher()
           params.forEach{ hash.combine($0) }
           return hash.finalize()
        }
        var uniqueKeys = Set<AnyHashable>()
        return filter{uniqueKeys.insert(keyValue(makeHash,$0)).inserted}
    }
}
