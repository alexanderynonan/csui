//
//  CSAttributedString.swift
//  CSUtilities
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    
    public func getSizeToWidth(_ width: CGFloat) -> CGSize {
        
        let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        return self.boundingRect(with: size, options: [.usesLineFragmentOrigin], context: nil).size
    }
    
    public func getSizeToHeight(_ height: CGFloat) -> CGSize {
        
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        return self.boundingRect(with: size, options: [.usesLineFragmentOrigin], context: nil).size
    }
    
    public class func createWith(text: String, font: UIFont = UIFont.systemFont(ofSize: UIFont.systemFontSize), color: UIColor = .black, lineSpacing: CGFloat = 0) -> NSAttributedString {
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing
        
        let dicAttributes = [NSAttributedString.Key.font : font,
                             NSAttributedString.Key.foregroundColor : color,
                             NSAttributedString.Key.paragraphStyle: style]
        
        
        let attribute = NSAttributedString(string: text, attributes: dicAttributes)
        return attribute
    }

}

extension NSMutableAttributedString {
    
    public func addLineSpacingWithValue(_ lineSpacing: CGFloat) {
        
        let range = NSRange(location: 0, length: self.length)
               
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        self.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: range)
    }
}
