//
//  CSDateManager.swift
//  CSUtilities
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import Foundation

public struct DifferenceDates{
    
    public var year    = 0
    public var month   = 0
    public var day     = 0
    public var hour    = 0
    public var minute  = 0
    public var second  = 0
}

public enum LocalIdentifier : String {
    
    case spanish_Peru    = "es_PE"
    case english         = "en"
}
//MARK: - class CSDateManager

public class CSDateManager {
    
    public class func convertDateInTextLocale(date : Date,dateStyle : DateFormatter.Style,language : LocalIdentifier? = nil) -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: language?.rawValue ?? "es_PE")
        dateFormatter.dateStyle = dateStyle
        
        return dateFormatter.string(from: date)
    }
    
    public class func convertDateIntTextTime(date : Date, format : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
}


extension Date{
    
    static public func convertTimestampInDate(_ timestamp: String) -> Date {
        
        if let interval = TimeInterval(timestamp) {
            return Date(timeIntervalSince1970: interval)
        }
        
        return Date()
    }
    
    static public func convertDateText(_ dateText: String, inDateWithFormat format: String) -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: Locale.current.identifier)
        dateFormatter.dateFormat = format
        
        return dateFormatter.date(from: dateText) ?? Date()
    }
    
    public func convertDate(inDateWithFormat format: String, withLocale locale: Locale = Locale(identifier: Locale.current.identifier)) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    static public func getYearsDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.year], from: initialDate, to: finalDate)
        return TimeInterval(components.year ?? 0)
    }
    
    static public func getMonthDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.month], from: initialDate, to: finalDate)
        return TimeInterval(components.month ?? 0)
    }
    
    static public func getDayDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.day], from: initialDate, to: finalDate)
        return TimeInterval(components.day ?? 0)
    }
    
    static public func getHourDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.hour], from: initialDate, to: finalDate)
        return TimeInterval(components.hour ?? 0)
    }
    
    static public func getMinuteDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.minute], from: initialDate, to: finalDate)
        return TimeInterval(components.minute ?? 0)
    }
    
    static public func getSecondDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> TimeInterval{
        
        let components = Calendar.current.dateComponents([.second], from: initialDate, to: finalDate)
        return TimeInterval(components.second ?? 0)
    }
    
    static public func getDifferenceBetweenDate(_ initialDate: Date, andFinalDate finalDate: Date) -> DifferenceDates{
        
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: initialDate, to: finalDate)
        
        var difference = DifferenceDates()
        
        difference.year     = components.year ?? 0
        difference.month    = components.month ?? 0
        difference.year     = components.day ?? 0
        difference.hour     = components.hour ?? 0
        difference.minute   = components.minute ?? 0
        difference.second   = components.second ?? 0
        
        return difference
    }
    
    static public func addTime(_ time: DateComponents, inDate date: Date) -> Date{
        
        return Calendar.current.date(byAdding: time, to: date) ?? Date()
    }
}
