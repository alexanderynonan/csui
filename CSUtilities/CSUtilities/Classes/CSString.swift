//
//  CSString.swift
//  CSUtilities
//
//  Created by Alexander Ynoñan H. on 22/03/21.
//  Copyright © 2021 Alexander Ynoñan H. All rights reserved.
//

import UIKit
import Foundation
import CommonCrypto

extension String {

//    DOUBLE
    public func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
//    IS NUMBER
    public var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    //OPEN URL
    public func openScheme(){
        if let scheme = self.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: scheme){
            UIApplication.shared.open(url, options: [:], completionHandler: {
                (success) in
                print("Open \(self): \(success)")
            })
        }
    }
    
    public var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    
    /// Returns a new `String` made by removing the whitespaces from both ends of the string.
    public var trim: String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    /// Returns a `String` depending on the current system language.
    public var localized: String {
        
        let languageString = "\((UserDefaults.standard.object(forKey: "AppleLanguages") as? [String])?.first?.split(separator: "-").first ?? "es")"
        let stringPath = Bundle.main.path(forResource: languageString, ofType: "lproj") ?? ""
        let bundle = Bundle(path: stringPath) ?? Bundle.main
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
    
    /**
     Returns a  new `Data` object made by decoding the string in base64.
     
     - Returns: Returns an optional `Data` object, can be `nil`.
     */
    public func base64Decode() -> Data? {
        
        var base64 = self
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        
        let length = Double(base64.lengthOfBytes(using: String.Encoding.utf8))
        let requiredLength = 4 * ceil(length / 4.0)
        let paddingLength = requiredLength - length
        
        if paddingLength > 0 {
            let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
            base64 += padding
        }
        
        return Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
    }
    
    /**
     Returns a `Bool` value that indicates if the string have a valid email address. It consists of and email prefix and an email domain, both in acceptable formats.
     
     - Parameters:
        - strictFilter: With this parameter you can set if the function uses an strict filter or not, for default is `True`.
     */
    public func mailIsValid(strictFilter: Bool = true) -> Bool {
        
        let strictRegex = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let normalRegex = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        
        let mailRegex = strictFilter ? strictRegex : normalRegex
        
        let predicate = NSPredicate(format: "SELF MATCHES %@", mailRegex)
        return predicate.evaluate(with: self)
    }
    
    /**
     Returns a new `String` made by replacing characters that are within a specific range by specific text.
     
     - Returns: Returns a `String`.
     
     - Parameters:
        - string:   String that will replace characters.
        - range:    Range that determines the characters to replace.
     */
    public func replaceByString(_ string: String, inRange range: CountableClosedRange<Int>) -> String {
        
        let start = index(string.startIndex, offsetBy: range.lowerBound)
        let end   = index(start, offsetBy: range.count)
        
        return self.replacingCharacters(in: start ..< end, with: string)
    }
    
    public func replaceSpecialCharacters() -> String{
        
        var result = self
        let replacements = ["Ñ": "N", "ñ": "n",
                            "Á": "A", "á": "a",
                            "É": "E", "é": "e",
                            "Í": "I", "í": "i",
                            "Ó": "O", "ó": "o",
                            "Ú": "U", "ú": "u"]
        
        replacements.keys.forEach { result = result.replacingOccurrences(of: $0, with: replacements[$0]!) }
        return result
    }
    
    /**
     Returns a new `String` encrypt
     
     - Returns: Returns a `String`.
     
     - Parameters:
        - key:   Key
        - iv:    Iv
        - options: kCCOptionPKCS7Padding
     */
    public func aes256CBCEncrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> String? {
        if let keyData = key.data(using: String.Encoding.utf8),
            let data = self.data(using: String.Encoding.utf8),
            let cryptData    = NSMutableData(length: Int((data.count)) + kCCBlockSizeAES128) {
            
            let keyLength              = size_t(kCCKeySizeAES256)
            let operation: CCOperation = UInt32(kCCEncrypt)
            let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES)
            let options:   CCOptions   = UInt32(options)
            
            var numBytesEncrypted :size_t = 0
            
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      iv,
                                      (data as NSData).bytes, data.count,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if cryptStatus == kCCSuccess {
                cryptData.length = Int(numBytesEncrypted)
                let base64cryptString = cryptData.base64EncodedString()
                return base64cryptString
            }
            else {
                return nil
            }
        }
        return nil
    }
    
    /**
     Returns a new `String` Decrypt
     
     - Returns: Returns a `String`.
     
     - Parameters:
        - key:   Key
        - iv:    Iv
        - options: kCCOptionPKCS7Padding
     */
    
    public func aes256CBCDecrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> String? {
        
        if let keyData = key.data(using: String.Encoding.utf8),
            let data = NSData(base64Encoded: self, options: .ignoreUnknownCharacters),
            let cryptData    = NSMutableData(length: Int((data.length)) + kCCBlockSizeAES128) {
            
            let keyLength              = size_t(kCCKeySizeAES256)
            let operation: CCOperation = UInt32(kCCDecrypt)
            let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES)
            let options:   CCOptions   = UInt32(options)
            
            var numBytesEncrypted :size_t = 0
            
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      iv,
                                      data.bytes, data.length,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let unencryptedMessage = String(data: cryptData as Data, encoding:String.Encoding.utf8)
                return unencryptedMessage
            }
            else {
                return nil
            }
        }
        return nil
    }
    
    var url: URL? {
        return URL(string: self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? self)
    }
}

public extension Double {
    
    func toInt() -> Int? {
        guard (self <= Double(Int.max).nextDown) && (self >= Double(Int.min).nextUp) else {
            return nil
        }
        return Int(self)
    }
    
    func toTwoDecimalStr() -> String?{
        return String(format: "%.2f", self)
    }
    
    func toThreeDecimalStr() -> String?{
        return String(format: "%.3f", self)
    }
    
    func toFourDecimalStr() -> String?{
        return String(format: "%.4f", self)
    }
    
    func toFiveDecimalStr() -> String?{
        return String(format: "%.5f", self)
    }
}
