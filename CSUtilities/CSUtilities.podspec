Pod::Spec.new do |s|
  s.name                  = 'CSUtilities'
  s.version               = '1.0.0'
  s.platform              = :ios
  s.ios.deployment_target = '11.0'

  # Files reference
  s.source_files          = 'CSUtilities/Classes/*'

  # Dependency
  
  # Podspec description
  s.summary               = 'A short description of CSUtilities.'
  s.source                = { :git => 'https://alexanderynonan/CSUtilities.git', :tag => s.version.to_s }
  s.homepage              = 'https://alexanderynonan/CSUtilities'
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { 'Alexander Ynoñan' => 'alexanderynonan@gmail.com' }
end
