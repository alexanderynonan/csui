# CSUI #

Clases generales reutilizables para futuros proyectos de IOS


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

To install it, simply add the following line to your Podfile:

```ruby

pod 'CSUI' 			, :path => 'csui/CSUI/'
pod 'CSUtilities' 	, :path => 'csui/CSUtilities/'

```

## Author

Alexander Johel Ynoñan Huayllapuma, Email: alexanderynonan@gmail.com, Phone: +51 987677106, Country: Perú 🇵🇪


## License

CSUI is available under the MIT license. See the LICENSE file for more info.
